package hello.service;

import hello.helper.ConverterForSorting;
import hello.model.DirectoryRecord;
import hello.model.DirectoryContentRecord;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;


/**
 * Сервис для работы с БД
 */
public class MysqlService {

    private JdbcTemplate jdbcTemplate;
    private ConverterForSorting converterForSorting = new ConverterForSorting();

    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /**
     * Добавление директории с вложенными сущностями
     *
     * @param dir
     */
    public void addDir(DirectoryRecord dir) {
        GeneratedKeyHolder holder = new GeneratedKeyHolder();

        // сохранение самой директории
        jdbcTemplate.update(con -> {
            PreparedStatement statement = con.prepareStatement("INSERT INTO dir (name, create_datetime) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, dir.getName());
            statement.setDate(2, dir.getCreateDate());
            return statement;
        }, holder);
        int dirId = holder.getKey().intValue();

        // формирование данных для получения сортировочной последовательности
        List<DirectoryContentRecord> listDirFile = dir.getListDirFile();
        String[] fileNames = new String[listDirFile.size()];
        for (int i = 0; i < listDirFile.size(); i++) {
            fileNames[i] = listDirFile.get(i).getName();
        }
        this.converterForSorting.setDataList(fileNames);

        // сохранение вложений файлов и директорий
        for (DirectoryContentRecord directoryContentRecord : dir.getListDirFile()) {
            directoryContentRecord.setDirId(dirId);
            directoryContentRecord.setOrd(this.converterForSorting.getStringForSorting(directoryContentRecord.getName()));
            this.addDirectoryContentRecord(directoryContentRecord);
        }
    }


    /**
     * Добавление вложенных файлов и директорий
     *
     * @param directoryContentRecord
     */
    public void addDirectoryContentRecord(DirectoryContentRecord directoryContentRecord) {
        GeneratedKeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(con -> {
            PreparedStatement statement = con.prepareStatement("INSERT INTO dir_file (dir_id, name, type, size, ord) VALUES (?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, directoryContentRecord.getDirId());
            statement.setString(2, directoryContentRecord.getName());
            statement.setString(3, directoryContentRecord.getType());
            statement.setLong(4, directoryContentRecord.getSize());
            statement.setString(5, directoryContentRecord.getOrd());
            return statement;
        }, holder);
    }


    /**
     * Получение списка директорий
     *
     * @return
     */
    public List<DirectoryRecord> getListDir() {
        List<DirectoryRecord> listDir = this.jdbcTemplate.query(
                "SELECT id, name, create_datetime, cnt_dir, cnt_file, size FROM v_dir",
                new DirectoryRowMapper());
        return listDir;
    }


    /**
     * Получение вложенных файлов и директорий
     *
     * @param dir
     * @return
     */
    public List<DirectoryContentRecord> getListDirFileByDir(DirectoryRecord dir) {
        List<DirectoryContentRecord> listDirFile = this.jdbcTemplate.query(
                "select id, name, type, size from dir_file where dir_id = ? order BY IF(type = 'dir', 0, 1), ord",
                new Object[]{dir.getId()},
                new DirectoryContentRowMapper());
        return listDirFile;
    }


    /**
     * Получение директории по ID
     *
     * @param id
     * @return
     */
    public DirectoryRecord getDirById(int id) {
        DirectoryRecord dir = (DirectoryRecord) this.jdbcTemplate.queryForObject(
                "SELECT id, name, create_datetime, cnt_dir, cnt_file, size FROM v_dir WHERE id = ?",
                new Object[]{id},
                new DirectoryRowMapper());
        return dir;
    }

}
