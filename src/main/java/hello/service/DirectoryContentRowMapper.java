package hello.service;

import java.sql.ResultSet;
import java.sql.SQLException;

import hello.model.DirectoryContentRecord;
import org.springframework.jdbc.core.RowMapper;

class DirectoryContentRowMapper implements RowMapper {
    public DirectoryContentRecord mapRow(ResultSet rs, int rowNum) throws SQLException {
        DirectoryContentRecord dirFile = new DirectoryContentRecord();
        dirFile.setId(rs.getInt("id"));
        dirFile.setName(rs.getString("name"));
        dirFile.setType(rs.getString("type"));
        dirFile.setSize(rs.getLong("size"));
        return dirFile;
    }
}
