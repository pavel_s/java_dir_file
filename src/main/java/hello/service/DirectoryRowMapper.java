package hello.service;

import java.sql.ResultSet;
import java.sql.SQLException;

import hello.model.DirectoryRecord;
import org.springframework.jdbc.core.RowMapper;

class DirectoryRowMapper implements RowMapper {
    public DirectoryRecord mapRow(ResultSet rs, int rowNum) throws SQLException {
        DirectoryRecord dir = new DirectoryRecord();
        dir.setId(rs.getInt("id"));
        dir.setName(rs.getString("name"));
        dir.setCreateDate(rs.getDate("create_datetime"));
        dir.setCountDir(rs.getInt("cnt_dir"));
        dir.setCountFile(rs.getInt("cnt_file"));
        dir.setSize(rs.getLong("size"));
        return dir;
    }
}
