package hello.helper;

import org.thymeleaf.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Конвертер для формирования строки, по которой можно сортировать
 */
public class ConverterForSorting {
    private int maxLength = 0;
    private String s0 = "";
    private Pattern pattern = Pattern.compile("(\\D+)|(\\d+)");

    /**
     * Передаем список строк для нахождения максимальной длинны циферной последовательности
     * @param strings
     */
    public void setDataList(String[] strings) {
        for (String str : strings) {
            Matcher m = this.pattern.matcher(str);
            while (m.find()) {
                String s = m.group(2);
                if (s != null) {
                    this.maxLength = Math.max(this.maxLength, s.length());
                }
            }
        }
    }


    /**
     * Приведение строки к нужному виду
     * @param str
     * @return
     */
    public String getStringForSorting(String str) {
        StringBuilder stringPath = new StringBuilder();
        Matcher m = this.pattern.matcher(str);
        while (m.find()) {
            String s = m.group(2);
            stringPath.append(s != null ? (StringUtils.repeat("0", maxLength - s.length()) + s) : m.group());
        }
        return stringPath.toString().toLowerCase();
    }

}
