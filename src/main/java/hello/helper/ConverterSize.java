package hello.helper;

/**
 * Конвертер для преобразования размеров файлов и директорий
 */
public class ConverterSize {

    public static String sizeToString(Long size) {
        if (size > 0) {
            String t[] = {"b", "Kb", "Mb", "Gb", "Tb"};
            int i = Math.min((int) Math.floor(Math.log(size) / Math.log(1024)), t.length - 1);
            return String.format("%.2f", (float) size / Math.pow(1024, i)) + " " + t[i];
        } else {
            return "";
        }
    }

}
