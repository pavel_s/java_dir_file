package hello.controller;

import hello.model.DirectoryContentRecord;
import hello.model.DirectoryRecord;
import hello.service.MysqlService;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.File;
import java.util.List;

@Controller
public class DirectoryController {
    private final MysqlService dataService;

    public DirectoryController() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        this.dataService = (MysqlService) context.getBean("mysqlService");
    }

    @RequestMapping(value = "/dirs_and_files", method = RequestMethod.GET)
    public String listDirectory(Model model) {
        List<DirectoryRecord> listDir = this.dataService.getListDir();
        model.addAttribute("listDir", listDir);
        return "dirs";
    }

    @RequestMapping(value = "/dirs_and_files", method = RequestMethod.POST)
    public String addDirectory(@RequestParam(value = "dir_name") String dirName, Model model) {
        try {
            File file = new File(dirName);
            DirectoryRecord directoryRecord = DirectoryRecord.create(file);
            this.dataService.addDir(directoryRecord);
            return "redirect:/dirs_and_files";
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "dir_error";
        }
    }

    @RequestMapping(value = "/dir/{id}", method = RequestMethod.GET)
    public String viewDirectory(@PathVariable("id") int id, Model model) {
        DirectoryRecord directoryRecord = this.dataService.getDirById(id);
        if (directoryRecord != null) {
            List<DirectoryContentRecord> listDirFile = this.dataService.getListDirFileByDir(directoryRecord);
            model.addAttribute("dir", directoryRecord);
            model.addAttribute("listDirFile", listDirFile);
            return "files";
        } else {
            model.addAttribute("error", "Директория не найдена");
            return "dir_error";
        }
    }

}
