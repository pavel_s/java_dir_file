package hello.model;

import hello.helper.ConverterSize;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DirectoryContentRecord {
    private int id;
    private String name;
    private long size = 0;
    private int dirId;
    private String type;
    private String ord;

    public int getDirId() {
        return dirId;
    }

    public void setDirId(int dir_id) {
        this.dirId = dir_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSize() {
        return this.size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getOrd() {
        return ord;
    }

    public void setOrd(String ord) {
        this.ord = ord;
    }

    public String getSizeString() {
        return ConverterSize.sizeToString(this.size);
    }

    /**
     * Создание объекта DirectoryContentRecord из объекта файловой системы
     *
     * @param file
     * @return
     */
    public static DirectoryContentRecord create(File file) {
        DirectoryContentRecord directoryContentRecord = new DirectoryContentRecord();
        directoryContentRecord.setName(file.getName());
        directoryContentRecord.setType(file.isDirectory() ? "dir" : "file");
        directoryContentRecord.setSize(file.isFile() ? file.length() : 0);
        return directoryContentRecord;
    }

}
