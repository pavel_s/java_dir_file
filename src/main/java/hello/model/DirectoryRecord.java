package hello.model;

import hello.helper.ConverterSize;

import java.io.File;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class DirectoryRecord {
    private int id;
    private String name;
    private long size = 0;
    private Date createDate;
    private List<DirectoryContentRecord> listDirFile;
    private int countDir;
    private int countFile;

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public int getCountDir() {
        return countDir;
    }

    public void setCountDir(int countDir) {
        this.countDir = countDir;
    }

    public int getCountFile() {
        return countFile;
    }

    public void setCountFile(int countFile) {
        this.countFile = countFile;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSize() {
        return this.size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public List<DirectoryContentRecord> getListDirFile() {
        return listDirFile;
    }

    public void setListDirFile(List<DirectoryContentRecord> listDirFile) {
        this.listDirFile = listDirFile;
    }

    public String getSizeString() {
        return ConverterSize.sizeToString(this.size);
    }

    public String getCreateDateString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        return sdf.format(this.createDate);
    }

    /**
     * Создание объекта DirectoryRecord из объекта файловой системы
     *
     * @param file
     * @return
     */
    public static DirectoryRecord create(File file) {
        if (!file.exists()) {
            throw new RuntimeException("Путь к папке не существует");
        }
        if (!file.isDirectory()) {
            throw new RuntimeException("Указаный путь не является директорией");
        }

        DirectoryRecord directoryRecord = new DirectoryRecord();
        directoryRecord.setName(file.getPath());
        directoryRecord.setCreateDate(new java.sql.Date(new java.util.Date().getTime()));

        // добавление вложенных элементов
        List<DirectoryContentRecord> listDirFile = new ArrayList<>();
        for (File childFile : file.listFiles()) {
            listDirFile.add(DirectoryContentRecord.create(childFile));
        }
        directoryRecord.setListDirFile(listDirFile);

        return directoryRecord;
    }
}
