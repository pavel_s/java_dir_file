/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
SET NAMES 'utf8';
USE test;

DROP TABLE IF EXISTS dir;
CREATE TABLE dir (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  create_datetime DATETIME NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = INNODB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

DROP TABLE IF EXISTS dir_file;
CREATE TABLE dir_file (
  id INT(11) NOT NULL AUTO_INCREMENT,
  dir_id INT(11) NOT NULL,
  name VARCHAR(255) NOT NULL,
  type ENUM('dir','file') NOT NULL,
  size BIGINT(20) DEFAULT NULL,
  ord TEXT DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FK_dir_file_dir_id FOREIGN KEY (dir_id)
  REFERENCES dir(id) ON DELETE CASCADE ON UPDATE CASCADE
)
  ENGINE = INNODB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;


DROP VIEW IF EXISTS v_dir CASCADE;
CREATE OR REPLACE
VIEW v_dir
AS
  select `dir`.`id` AS `id`,`dir`.`name` AS `name`,`dir`.`create_datetime` AS `create_datetime`,
    (select count(1) from `dir_file` where ((`dir_file`.`dir_id` = `dir`.`id`) and (`dir_file`.`type` = 'dir'))) AS `cnt_dir`,
    (select count(1) from `dir_file` where ((`dir_file`.`dir_id` = `dir`.`id`) and (`dir_file`.`type` = 'file'))) AS `cnt_file`,
    coalesce((select sum(`dir_file`.`size`) from `dir_file` where ((`dir_file`.`dir_id` = `dir`.`id`) and (`dir_file`.`type` = 'file'))),0) AS `size`
  from `dir` order by `dir`.`create_datetime` desc;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;